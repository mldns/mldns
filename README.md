# Machine Learning DNS 

This [MLDNS](https://gitlab.com/mldns/mldns) Repository gives you an overview over all repositories of the **Machine Learning DNS** project.

## Research Subject

DNS (Domain Name Service) is an essential Internet service and also offers excellent opportunities as a security tool.
We protect home networks by analyzing the DNS traffic using machine learning techniques.
The approach of *Machine Learning DNS (MLDNS)* is to train a classifier to distinguish between clean domains names and malware domains, which goes beyond static blacklists.
We integrate the classifier into widely used DNS software (*unbound*), which can run on a CPE router.
Subsequently, the traffic to domains that are classified as malicious is blocked.

[![Machine Learning DNS](mldns.png)](https://gitlab.com/mldns/mldns/-/raw/master/mldns.mp4 "Machine Learning DNS")

## Reports

Two reports were prepared along with this research project.

1. [Machine Learning DNS - Making home networks secure with AI](https://mldns.gitlab.io/ml-report/ml-report.pdf)
2. [Evaluation of DNS-filtering with real-time ML predictions](https://mldns.gitlab.io/dns-report/dns-report.pdf)

## Repositories

For more information on machine learning, the two repositories [FastText](https://gitlab.com/mldns/fasttext) and [TensorFlow](https://gitlab.com/mldns/tensorflow), named after the underlying technique, are good places to start.

A good start to the topic of DNS evaluation and integration would be in the [mldns-meta](https://gitlab.com/mldns/mldns-meta) repository.

A complete list of all repositories can be found [here](https://gitlab.com/mldns).

## Licenses

License information can be found in each repository.
